import com.us.awpto.MathFunctions;
import org.junit.Assert;
import org.junit.Test;

public class CityBlockDistanceTest {
    @Test
    public void distanceTest() {
        MathFunctions mathFunctions = new MathFunctions();
        Assert.assertEquals( 0, mathFunctions.cityBlockDistance(1,2, 1,2)) ;
        Assert.assertEquals(3, mathFunctions.cityBlockDistance(1, 1, 2, 3));
        Assert.assertEquals(0, mathFunctions.cityBlockDistance(0, 0, 0, 0));
    }

}
