import com.us.awpto.Board;
import com.us.awpto.GameObjectNameEnum;
import com.us.awpto.Starship;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FigthTest {

    Board board;
    Starship player;
    Starship klingon;

    @Before
    public void init(){
        board = new Board();
        player = new Starship(board, 20, 600, GameObjectNameEnum.ENTERPRISE);
        klingon = new Starship(board, 20, 100, GameObjectNameEnum.KLINGON);
    }

    @Test
    public void shouldConsumeEnergyWhileAttack() {
        player.setNewPosition(3, 3, 3, 3);
        player.setEnergyUnit(600);
        klingon.setNewPosition(3, 3, 5, 5);
        klingon.setEnergyUnit(100);
        player.attack(klingon, 50);

        assertEquals(50, klingon.getEnergyUnit());
        assertEquals(550, player.getEnergyUnit());
    }

    @Test
    public void shouldDestroyEnemyAndConsumeEnergy() {
        player.setNewPosition(3, 3, 3, 3);
        klingon.setNewPosition(3, 3, 5, 5);
        player.setEnergyUnit(600);
        klingon.setEnergyUnit(100);
        player.attack(klingon, 100);

        assertTrue(klingon.isDestroyed());
        assertEquals(500, player.getEnergyUnit());
    }
}
