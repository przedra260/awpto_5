import com.us.awpto.Board;
import com.us.awpto.GameObject;
import com.us.awpto.GameObjectNameEnum;
import com.us.awpto.Starship;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {

    Board board;
    Starship klingon;
    Starship player;
    GameObject star;
    GameObject starFleet;

    @Before
    public void init(){
        board = new Board();
        klingon = new Starship(board, 20, 600, GameObjectNameEnum.KLINGON);
        player = new Starship(board, 30, 600, GameObjectNameEnum.ENTERPRISE);
        starFleet = new GameObject(board, GameObjectNameEnum.STARFLEET);
        star = new GameObject(board, GameObjectNameEnum.STAR);
    }

    @Test
    public void testLongScan(){
        klingon.setNewPosition(3, 3, 1, 1);
        starFleet.setNewPosition(3 ,3 , 2 ,2);
        star.setNewPosition(2, 2, 5, 5);
        String actual = board.longScan(3, 3);


        int[][] klingonsNumber = new int[3][3];
        int[][] starFleetsNumber = new int[3][3];
        int[][] starsNumber = new int[3][3];
        klingonsNumber[1][1] = 1;
        starFleetsNumber[1][1] = 1;
        starsNumber[0][0] = 1;
        StringBuilder scanResult = new StringBuilder();
        for (int i = 2; i >= 0; i--) {
            for (int j = 0; j < 3; j++) {
                scanResult.append(klingonsNumber[i][j]).append(starFleetsNumber[i][j]).append(starsNumber[i][j]).append("\t");
            }
            scanResult.append("\n");
        }

        assertEquals(scanResult.toString(), actual);
    }

    @Test
    public void shouldExistKlingonsBeyondCurrentQuadrant() {
        board = new Board();
        klingon.setBoard(board);
        player.setBoard(board);
        klingon.setNewPosition(1, 2, 3, 3);
        player.setNewPosition(3, 3, 3, 3);

        assertTrue(board.checkIfKlingonsExistBeyondCurrentQuadrant(player.getQuadrant_x(), player.getQuadrant_y()));

    }

    @Test
    public void shouldNotExistKlingonsBeyondCurrentQuadrant() {
        board = new Board();
        klingon.setBoard(board);
        player.setBoard(board);
        klingon.setNewPosition(3, 3, 4, 5);
        player.setNewPosition(3, 3, 3, 3);

        assertFalse(board.checkIfKlingonsExistBeyondCurrentQuadrant(player.getQuadrant_x(), player.getQuadrant_y()));

    }
}
