import com.us.awpto.Board;
import com.us.awpto.GameObject;
import com.us.awpto.GameObjectNameEnum;
import com.us.awpto.Starship;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StarshipTest {

    Board board;
    Starship player;
    GameObject starFleet;
    List<Starship> klingons = new ArrayList<>();

    @Before
    public void init(){
        board = new Board();
        starFleet = new GameObject(board, GameObjectNameEnum.STARFLEET);
        player = new Starship(board, 20, 600, GameObjectNameEnum.ENTERPRISE);
        for (int i=0; i<7; i++){
            klingons.add(new Starship(board, 20, 100, GameObjectNameEnum.KLINGON));
        }
    }

    @Test
    public void shouldMoveToRightSector() {
        player.setNewPosition(0, 0, 1, 1);

        player.moveShip(1, 0);
        assertEquals(2, player.getSector_x());
        assertEquals(1, player.getSector_y());
        player.moveShip(-1, 0);
        assertEquals(1, player.getSector_x());
        assertEquals(1, player.getSector_y());
        player.moveShip(0, 1);
        assertEquals(1, player.getSector_x());
        assertEquals(2, player.getSector_y());
        player.moveShip(0, -1);
        assertEquals(1, player.getSector_x());
        assertEquals(1, player.getSector_y());
    }

    @Test
    public void shouldMoveToRightQuadrant() {
        player.setNewPosition(1, 1, 0, 0);

        player.moveShip(-1, 0);
        assertEquals(0, player.getQuadrant_x());
        assertEquals(1, player.getQuadrant_y());
        player.moveShip(1, 0);
        assertEquals(1, player.getQuadrant_x());
        assertEquals(1, player.getQuadrant_y());
        player.moveShip(0, -1);
        assertEquals(1, player.getQuadrant_x());
        assertEquals(0, player.getQuadrant_y());
        player.moveShip(0, 1);
        assertEquals(1, player.getQuadrant_x());
        assertEquals(1, player.getQuadrant_y());
    }

    @Test
    public void shouldConsumeEnergyInCaseSectorChange(){
        player.setNewPosition(0, 0, 0, 0);
        player.setEnergyUnit(600);

        player.moveShip(2, 3);
        assertEquals(595, player.getEnergyUnit());
    }

    @Test
    public void shouldConsumeStellarDatesInCaseQuadrantChange(){
        player.setNewPosition(0, 0, 7, 7);
        player.setStellarDates(20);

        player.moveShip(1 ,1);
        assertEquals(18, player.getStellarDates());
    }

    @Test
    public void shouldConsumeAppropriateValueOfEnergy() {
        player.setEnergyUnit(600);
        player.setStellarDates(20);
        player.consumeEnergy(100, 10);

        assertEquals(500, player.getEnergyUnit());
        assertEquals(10, player.getStellarDates());
    }

    @Test
    public void shouldNotMoveOverGalaxy(){
        player.setNewPosition(0, 0, 0, 0);

        player.moveShip(-1, 0);
        assertEquals(0, player.getQuadrant_x());
        assertEquals(0, player.getQuadrant_y());
        player.moveShip(0, -1);
        assertEquals(0, player.getQuadrant_x());
        assertEquals(0, player.getQuadrant_y());
    }

    @Test
    public void shouldFindEnemy() {
        player.setNewPosition(4, 4, 3, 3);
        klingons.get(3).setNewPosition(4, 4, 2, 1);
        Starship enemy = player.findEnemy(klingons);

        assertEquals(klingons.get(3), enemy);
    }

    @Test
    public void shouldRefuelEnergy() {
        player.setNewPosition(4, 4, 3, 3);
        player.setEnergyUnit(344);
        starFleet.setNewPosition(4, 4, 3, 2);
        player.checkIfStarFleetIsNearbyAndRefuelIfIs();

        assertEquals(600, player.getEnergyUnit());
    }

    @Test
    public void shouldNotRefuelEnergy() {
        player.setNewPosition(4, 4, 3, 4);
        player.setEnergyUnit(344);
        starFleet.setNewPosition(4, 4, 3, 2);
        player.checkIfStarFleetIsNearbyAndRefuelIfIs();

        assertEquals(344, player.getEnergyUnit());
    }

    @Test
    public void shouldEnterpriseBeDestroyed() {
        player.setNewPosition(4, 4, 3, 4);
        player.setEnergyUnit(100);
        klingons.get(0).setNewPosition(4, 3, 3, 3);
        player.consumeEnergy(100);

        assertTrue(player.isDestroyed());
    }

}
