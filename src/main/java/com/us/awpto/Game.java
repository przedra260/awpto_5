package com.us.awpto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Getter
@Setter
public class Game {
    Board board;
    Starship player;
    List<Starship> klingons = new ArrayList<>();
    List<GameObject> starFleetBase = new ArrayList<>();
    List<GameObject> stars = new ArrayList<>();
    int mode = -1;
    boolean finishedGame = false;
    Scanner scan = new Scanner(System.in);

    public Game() {
    }

    public void initGame(){

        board = new Board();
        player = new Starship(board, 20, 600, GameObjectNameEnum.ENTERPRISE);
        player.setQuadrant_x(3);
        player.setQuadrant_y(3);
        player.setSector_x(7);
        player.setSector_y(7);
        player.setNewPosition(3, 3, 7, 7);

        for (int i=0; i<7; i++){
            klingons.add(new Starship(board, 20, 100, GameObjectNameEnum.KLINGON));
        }
        for (int i=0; i<2; i++){
            starFleetBase.add(new GameObject(board, GameObjectNameEnum.STARFLEET));
        }
        for (int i=0; i<20; i++){
            stars.add(new GameObject(board, GameObjectNameEnum.STAR));
        }
    }

    public void readCommand() {
        System.out.println("Podaj komende (0, 1 ,2 ,3, 4)");
        scan = new Scanner(System.in);
        mode = Integer.parseInt(scan.nextLine());
        executeCommand();
    }

    private void executeCommand() {
        switch (mode) {
            case 0: {
                System.out.println("VECTOR ? ");
                System.out.print("x -> ");
                int x = scan.nextInt();
                System.out.print("y -> ");
                int y = scan.nextInt();
                player.moveShip(x, y);
                scan = new Scanner(System.in);
                break;
            }
            case 1: {
                for (int i = 7; i>=0; i--) {
                    for (int j = 0; j<8; j++) {
                        System.out.print(board.getBoard()[player.getQuadrant_x()][player.getQuadrant_y()][j][i]);
                    }
                    System.out.println();
                }
                break;
            }
            case 2: {
                board.longScan(player.getQuadrant_x(), player.getQuadrant_y());
                break;
            }
            case 3: {
                fight();
                break;
            }
            case 4: {
                board.getScanHistory().forEach(System.out::println);
                break;
            }
            default: finishedGame = true;
        }
    }

    private void fight() {
        Starship enemy = player.findEnemy(klingons);
        if (enemy != null) {
            System.out.println("FIGHT MODE");
            showStatus();
            System.out.println("Enter number of energy to attack");
            int energy = scan.nextInt();
            if (energy == 0) return;
            if (player.attack(enemy, energy)) {
                System.out.println("SUCCESSFULL ATTACK");
            } else {
                System.out.println("MISSED");
            }
            if (enemy.isDestroyed()) {
                getBoard().removeObjectFromBoard(enemy);
                klingons.remove(enemy);
            }
        } else {
            System.out.println("ENEMY NOT FOUND");
        }
    }

    public boolean checkIfGameIsOver() {
        if (player.isDestroyed()) {
            System.out.println("GAME IS OVER");
            finishedGame = true;
            return true;
        }
        return false;
    }

    public void showStatus(){
        System.out.println("Enterprise status:\n - stellarDates = " + player.getStellarDates() + "\n - energyUnit = " + player.getEnergyUnit()
                + "\n quadrant = (" + player.getQuadrant_x() + "," + player.getQuadrant_y() + ")\n"
                + "sector = (" + player.getSector_x() + "," + player.getSector_y() + ")\n");
    }
}
