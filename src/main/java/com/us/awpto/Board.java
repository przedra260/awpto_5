package com.us.awpto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Board {


    private String[][][][] board = new String[8][8][8][8];
    private List<String> scanHistory = new ArrayList<String>();

    public Board() {
        initBoard();
    }

    private void initBoard(){
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                for (int k = 0; k < 8; k++){
                    for (int l = 0; l < 8; l++){
                        board[i][j][k][l] = GameObjectNameEnum.EMPTY.getObjectName();
                    }
                }
            }
        }
    }

    public void showBoard() {
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                for (int k = 0; k < 8; k++){
                    for (int l = 0; l < 8; l++){
                        System.out.print(board[k][i][j][l]);;
                    }
                }
                System.out.println();
            }
        }
    }

    public void removeObjectFromBoard(GameObject gameObject) {
        board[gameObject.getQuadrant_x()][gameObject.getQuadrant_y()][gameObject.getSector_x()][gameObject.getQuadrant_y()] = GameObjectNameEnum.EMPTY.getObjectName();
    }

    public String longScan(int quadrant_x, int quadrant_y){
        if (quadrant_x > 0) {
            quadrant_x--;
            if (quadrant_x + 2 > 7) {
                quadrant_x--;
            }
        }
        if (quadrant_y > 0) {
            quadrant_y--;
            if (quadrant_y + 2 > 7) {
                quadrant_y--;
            }
        }
        int[][] klingonsNumber = new int[3][3];
        int[][] starFleetsNumber = new int[3][3];
        int[][] starsNumber = new int[3][3];
        for (int i = quadrant_x; i < quadrant_x + 2 && i < 8; i++) {
            for (int j = quadrant_y; j < quadrant_y + 2 && j < 8; j++) {
                for (int k = 0; k < 8; k++) {
                    for (int l = 0; l < 8; l++) {
                        if (GameObjectNameEnum.KLINGON.getObjectName().equals(board[i][j][k][l])) {
                            klingonsNumber[i-quadrant_x][j-quadrant_y]++;
                        }
                        if (GameObjectNameEnum.STARFLEET.getObjectName().equals(board[i][j][k][l])) {
                            starFleetsNumber[i-quadrant_x][j-quadrant_y]++;
                        }
                        if (GameObjectNameEnum.STAR.getObjectName().equals(board[i][j][k][l])) {
                            starsNumber[i-quadrant_x][j-quadrant_y]++;
                        }
                    }
                }
            }
        }
        StringBuilder scanResult = new StringBuilder();
        for (int i = 2; i >= 0; i--) {
            for (int j = 0; j < 3; j++) {
                scanResult.append(klingonsNumber[i][j]).append(starFleetsNumber[i][j]).append(starsNumber[i][j]).append("\t");
            }
            scanResult.append("\n");
        }
        scanHistory.add(scanResult.toString());
        System.out.println(scanResult.toString());
        return scanResult.toString();
    }

    public boolean checkIfKlingonsExistBeyondCurrentQuadrant(int quadrant_x, int quadrant_y) {
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                for (int k = 0; k < 8; k++){
                    for (int l = 0; l < 8; l++){
                        if (GameObjectNameEnum.KLINGON.getObjectName().equals(board[i][j][k][l]) && i != quadrant_x &&  j != quadrant_y) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

}
