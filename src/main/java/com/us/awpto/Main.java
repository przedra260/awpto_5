package com.us.awpto;


public class Main {

    public static void main(String[] args) {

        Game game = new Game();
        game.initGame();

        do {
            if (game.checkIfGameIsOver()) break;
            game.showStatus();
            game.readCommand();

        } while (!game.isFinishedGame());
    }
}
