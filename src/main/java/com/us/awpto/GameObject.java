package com.us.awpto;

import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Getter
@Setter
public class GameObject {

    private Board board;
    private int quadrant_x = 0;
    private int quadrant_y = 0;
    private int sector_x = 0;
    private int sector_y = 0;
    private GameObjectNameEnum objectNameEnum;

    public GameObject(Board b, GameObjectNameEnum objectNameEnum) {
        this.board = b;
        this.objectNameEnum = objectNameEnum;
        Random random = new Random();
        while(true) {
            if (setNewPosition(random.nextInt(7), random.nextInt(7), random.nextInt(7), random.nextInt(7))) break;
        }
    }

    public boolean setNewPosition(int quadrant_x, int quadrant_y, int sector_x, int sector_y) {
        String[][][][] boardArr = this.board.getBoard();
        if (boardArr[quadrant_x][quadrant_y][sector_x][sector_y].equals(GameObjectNameEnum.EMPTY.getObjectName())) {
            boardArr[this.quadrant_x][this.quadrant_y][this.sector_x][this.sector_y] = "   ";
            this.quadrant_x = quadrant_x;
            this.quadrant_y = quadrant_y;
            this.sector_x = sector_x;
            this.sector_y = sector_y;
            boardArr[quadrant_x][quadrant_y][sector_x][sector_y] = this.objectNameEnum.getObjectName();
            this.board.setBoard(boardArr);
            return true;
        } else {
            System.out.println("YOU CAME TO THE PLACE OF ANOTHER OBJECT. GAME IS OVER");
            return false;
        }
    }
}
