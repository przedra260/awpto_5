package com.us.awpto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Starship extends GameObject{

    private int stellarDates = 0;
    private int energyUnit = 0;
    private boolean destroyed = false;

    public Starship(Board b, int stellarDates, int energyUnit, GameObjectNameEnum objectName) {
        super(b, objectName);
        this.stellarDates = stellarDates;
        this.energyUnit = energyUnit;
    }

    public void moveShip(int x, int y) {
        int sector_x = getSector_x();
        int sector_y = getSector_y();
        int quadrant_x = getQuadrant_x();
        int quadrant_y = getQuadrant_y();
        int usedStellarDates = 0;
        int usedEnergyUnit = 0;
        while(x > 0) {
            if(sector_x >= 7){
                if (quadrant_x < 7) {
                    quadrant_x++;
                    sector_x = 0;
                    usedStellarDates++;
                } else {
                    x = 0;
                    break;
                }
            } else {
                sector_x++;
            }
            usedEnergyUnit++;
            x--;
        }
        while(x < 0) {
            if(sector_x <= 0){
                if (quadrant_x > 0) {
                    quadrant_x--;
                    sector_x = 7;
                    usedStellarDates++;
                } else {
                    break;
                }
            } else {
                sector_x--;
            }
            usedEnergyUnit++;
            x++;
        }
        while(y > 0) {
            if(sector_y >= 7){
                if (quadrant_y < 7) {
                    quadrant_y++;
                    sector_y = 0;
                    usedStellarDates++;
                } else {
                    y = 0;
                    break;
                }
            }
            else {
                sector_y++;
            }
            usedEnergyUnit++;
            y--;
        }
        while(y < 0) {
            if(sector_y <= 0){
                if (quadrant_y > 0) {
                    quadrant_y--;
                    sector_y = 7;
                    usedStellarDates++;
                } else {
                    break;
                }
            } else {
                sector_y--;
            }
            usedEnergyUnit++;
            y++;
        }
        if (setNewPosition(quadrant_x, quadrant_y, sector_x, sector_y)) {
            consumeEnergy(usedEnergyUnit, usedStellarDates);
        } else {
            destroyShip();
        }
        checkIfStarFleetIsNearbyAndRefuelIfIs();
    }

    public Starship findEnemy(List<Starship> klingons) {
        MathFunctions mathFunctions = new MathFunctions();
        int minDist = 100;
        Starship nearestKlingon = null;
        List<Starship> filteredKlingons =  klingons.stream().filter(k -> k.getQuadrant_x() == getQuadrant_x()).filter(k -> k.getQuadrant_y() == getQuadrant_y()).collect(Collectors.toList());
        for (Starship klingon : filteredKlingons) {
            int dist = mathFunctions.cityBlockDistance(getSector_x(), getSector_y(), klingon.getSector_x(), klingon.getSector_y());
            if (dist < minDist) {
                minDist = dist;
                nearestKlingon = klingon;
            }
        }
        return nearestKlingon;
    }

    public boolean attack(Starship enemy, int energy) {
        double possibilityToAttack = 5.0 / (cityBlockDistance(getSector_x(), getSector_y(), enemy.getSector_x(), getSector_y()) + 4);
        consumeEnergy(energy, 0);
        if (Math.random() < possibilityToAttack) {
            enemy.consumeEnergy(energy);
            System.out.println();
            return true;
        } else {
            return false;
        }
    }

    public void consumeEnergy(int energyUnit) {
        consumeEnergy(energyUnit, 0);
    }

    public void consumeEnergy(int energyUnit, int stellarDates) {
        this.stellarDates -= stellarDates;
        if  (getObjectNameEnum().equals(GameObjectNameEnum.ENTERPRISE)
                && this.stellarDates <= 0 && getBoard().checkIfKlingonsExistBeyondCurrentQuadrant(getQuadrant_x(), getQuadrant_y())) {
            destroyShip();
        }
        this.energyUnit -= energyUnit;
        if (this.energyUnit <= 0) {
            destroyShip();
        }
    }

    public void checkIfStarFleetIsNearbyAndRefuelIfIs() {
        String klingonName = GameObjectNameEnum.STARFLEET.getObjectName();
        if ((getSector_x() > 0 && getBoard().getBoard()[getQuadrant_x()][getQuadrant_y()][getSector_x()-1][getSector_y()].equals(klingonName))
            || (getSector_y() < 7 && getBoard().getBoard()[getQuadrant_x()][getQuadrant_y()][getSector_x()][getSector_y()+1].equals(klingonName))
                || (getSector_x() < 7 && getBoard().getBoard()[getQuadrant_x()][getQuadrant_y()][getSector_x()+1][getSector_y()].equals(klingonName))
                || (getSector_y() > 0 && getBoard().getBoard()[getQuadrant_x()][getQuadrant_y()][getSector_x()][getSector_y()-1].equals(klingonName))) {

            refuelEnergy();
        }
    }

    public void refuelEnergy() {
        setEnergyUnit(600);
        System.out.println("ENERGY HAS BEEN REACHED");
    }

    public boolean setNewPosition(int quadrant_x, int quadrant_y, int sector_x, int sector_y) {
        return super.setNewPosition(quadrant_x, quadrant_y, sector_x, sector_y);
    }

    public int cityBlockDistance(int x1, int y1, int x2, int y2){
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    public void destroyShip(){
        this.destroyed = true;
        getBoard().removeObjectFromBoard(this);
    }

}
