package com.us.awpto;


public enum GameObjectNameEnum {
    EMPTY("   "),
    KLINGON("+++"),
    STARFLEET(">!<"),
    STAR(" * "),
    ENTERPRISE("<*>");

    private String objectName;

    GameObjectNameEnum(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectName() {
        return objectName;
    }

}
